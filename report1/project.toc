\contentsline {chapter}{List of Figures}{iv}% 
\contentsline {chapter}{List of Tables}{v}% 
\contentsline {chapter}{\numberline {1}INTRODUCTION}{1}% 
\contentsline {section}{\numberline {1.1}Overview}{1}% 
\contentsline {chapter}{\numberline {2}RELATED WORK}{2}% 
\contentsline {chapter}{\numberline {3}DESIGN and IMPLEMENTATION}{3}% 
\contentsline {section}{\numberline {3.1}Design}{3}% 
\contentsline {subsection}{\numberline {3.1.1}Logistic Regression}{4}% 
\contentsline {subsection}{\numberline {3.1.2}Support Vector Machine Classifier}{4}% 
\contentsline {subsubsection}{Tuning parameters: Kernel and Margin}{4}% 
\contentsline {subsubsection}{Implementation of argument mining using SVM}{5}% 
\contentsline {subsection}{\numberline {3.1.3}K-Means Clustering}{5}% 
\contentsline {subsubsection}{Implementation of argument mining using K-means clustering}{6}% 
\contentsline {subsection}{\numberline {3.1.4}Multinomial Naive Bayes Classifier}{6}% 
\contentsline {subsubsection}{Implementation of argument mining using Multinomial Naive Bayes Classifier}{7}% 
\contentsline {section}{\numberline {3.2}Implementation}{7}% 
\contentsline {chapter}{\numberline {4}TESTING}{8}% 
\contentsline {chapter}{\numberline {5}RESULTS}{9}% 
\contentsline {chapter}{\numberline {6}CONTRIBUTIONS}{10}% 
\contentsline {chapter}{\numberline {7}CONCLUSION}{11}% 
\contentsline {chapter}{Appendices}{13}% 
\contentsline {chapter}{\numberline {A}Sample Code}{14}% 
