import os
import numpy as np
import pandas as pd
from sklearn.feature_extraction.text import (CountVectorizer, TfidfTransformer)
from sklearn.svm import LinearSVC
from nltk.tokenize import sent_tokenize
from sklearn import metrics
from sklearn import svm
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.metrics import (confusion_matrix, classification_report, accuracy_score)
path = 'dataset.tsv'
data = pd.read_table(path,header=None,skiprows=1,names=['pro','con'])
print(data.head());
X = data.con
y = data.pro
vectorizer = CountVectorizer(stop_words='english', ngram_range = (1,2), max_df = 0.9, min_df = 3)
X_train, X_test, y_train, y_test = train_test_split(X,y,random_state=1, test_size= 0.2)
vectorizer.fit(X_train)
X_test_ = vectorizer.transform(X_test)
X_train_  = vectorizer.transform(X_train)
svclassifier = svm.SVC(kernel='linear', C=0.1)
svclassifier.fit(X_train_, y_train)
Prediction = svclassifier.predict(X_test_)
print('Support Vector Machine Accuracy: ',metrics.accuracy_score(y_test,Prediction)*100,'%',sep='')
plt.scatter(X,y)
plt.show()
Train = CountVectorizer(stop_words='english', ngram_range = (1,2), max_df = .90, min_df = 3)
Train.fit(X)
X_data = Train.transform(X)
svclassifierfinal = svm.SVC(kernel='linear')
svclassifierfinal.fit(X_data, y)
print('Enter sentence to be analysed: ', end=" ")
test = []
test.append(input())
test_dtm = Train.transform(test)
predLabel = svclassifierfinal.predict(test_dtm)
tags = ['negative','positive']
print('The argument is ',tags[predLabel[0]])
Train1 = CountVectorizer(stop_words='english', ngram_range = (1,2), max_df = .90, min_df = 3)
Train1.fit(X)
X1_data = Train.transform(X)
svclassifierfinal1 = svm.SVC(kernel='linear')
svclassifierfinal1.fit(X1_data, y)
file=open('test.txt','r');
lines=file.read();
length=len(lines);
stringarray = lines.split('.')
for i in stringarray:
    print(i);
    i=[i]
    test_dtm = Train.transform(i)
    predLabel = svclassifierfinal.predict(test_dtm)
    tags = ['negative','positive']
    print('The argument is ',tags[predLabel[0]])
