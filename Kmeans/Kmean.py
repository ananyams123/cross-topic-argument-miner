from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.neighbors import KNeighborsClassifier
from sklearn import metrics
from sklearn.cluster import KMeans
from nltk.corpus import stopwords
from nltk.stem.wordnet import WordNetLemmatizer
import string
import re
import numpy as np
from collections import Counter
 

stop = set(stopwords.words('english'))
exclude = set(string.punctuation)
lemma = WordNetLemmatizer()
 
def clean(doc):
    stop_free = " ".join([i for i in doc.lower().split() if i not in stop])
    punc_free = ''.join(ch for ch in stop_free if ch not in exclude)
    normalized = " ".join(lemma.lemmatize(word) for word in punc_free.split())
    processed = re.sub(r"\d+","",normalized)
    y = processed.split()
    return y



path = "/home/abhinav/Desktop/loveee/cross-topic-argument-miner/Kmeans/dataset.txt"


 
train_clean_sentences = []
fp = open(path,'r')
for line in fp:
    line = line.strip()
    cleaned = clean(line)
    cleaned = ' '.join(cleaned)
    train_clean_sentences.append(cleaned)

 
vectorizer = TfidfVectorizer(stop_words='english')
X = vectorizer.fit_transform(train_clean_sentences)
 


y_train = np.zeros(7086)
y_train[0:3996] = 1



modelkmeans = KMeans(n_clusters=2, init='k-means++', max_iter=10000, n_init=100)
modelkmeans.fit(X)





print("enter the argument")
test_sentence = []
test_sentence.append(input())
 
test_clean_sentence = []
for test in test_sentence:
    cleaned_test = clean(test)
    cleaned = ' '.join(cleaned_test)
    cleaned = re.sub(r"\d+","",cleaned)
    test_clean_sentence.append(cleaned)
 
Test = vectorizer.transform(test_clean_sentence)
 

true_test_labels = ['con','pro']
predicted_labels_kmeans = modelkmeans.predict(Test)
 
print("it is a ",true_test_labels[predicted_labels_kmeans[0]],"argument")
