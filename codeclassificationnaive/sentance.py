import pandas as pd
from sklearn.naive_bayes import MultinomialNB
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn import metrics

path = '/home/abhinav/Desktop/loveee/cross-topic-argument-miner/codeclassificationnaive/dataset.tsv'
data = pd.read_table(path,header=None,skiprows=1,names=['pro','con'])
X = data.con
y = data.pro



vect = CountVectorizer(stop_words='english', ngram_range = (1,1), max_df = .80, min_df = 4)
X_train, X_test, y_train, y_test = train_test_split(X,y,random_state=1, test_size= 0.2)

vect.fit(X_train)
X_train_dtm = vect.transform(X_train) 
X_test_dtm = vect.transform(X_test)

NB = MultinomialNB()
NB.fit(X_train_dtm, y_train)
y_pred = NB.predict(X_test_dtm)
print('\nNaive Bayes:')
print('Accuracy: ',metrics.accuracy_score(y_test,y_pred)*100,'%',sep='')

trainingVector = CountVectorizer(stop_words='english', ngram_range = (1,1), max_df = .80, min_df = 5)
trainingVector.fit(X)
X_dtm = trainingVector.transform(X)
NB_complete = MultinomialNB()
NB_complete.fit(X_dtm, y)


print('\nenter an argumentative sentence')
print('Enter sentence to be analysed: ', end=" ")
test = []
test.append(input())
test_dtm = trainingVector.transform(test)
predLabel = NB_complete.predict(test_dtm)
tags = ['con','pro']

print('The argument is predicted as',tags[predLabel[0]])


