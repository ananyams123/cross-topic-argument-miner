from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.neighbors import KNeighborsClassifier
from sklearn import metrics
from sklearn.cluster import KMeans
from nltk.corpus import stopwords
from nltk.stem.wordnet import WordNetLemmatizer
import string
import re
import numpy as np
from collections import Counter


stop = set(stopwords.words('english'))
exclude = set(string.punctuation)
lemma = WordNetLemmatizer()

def clean(doc):
    stop_free = " ".join([i for i in doc.lower().split() if i not in stop])
    punc_free = ''.join(ch for ch in stop_free if ch not in exclude)
    normalized = " ".join(lemma.lemmatize(word) for word in punc_free.split())
    processed = re.sub(r"\d+","",normalized)
    y = processed.split()
    return y



path = "./dataset1.txt"
path1 = "./dataset2.txt"


train_clean_sentences = []
fp = open(path,'r')
for line in fp:
    line = line.strip()
    cleaned = clean(line)
    cleaned = ' '.join(cleaned)
    train_clean_sentences.append(cleaned)

train_clean_sentences1 = []
fp = open(path1,'r')
for line in fp:
    line = line.strip()
    cleaned = clean(line)
    cleaned = ' '.join(cleaned)
    train_clean_sentences1.append(cleaned)


vectorizer = TfidfVectorizer(stop_words='english')
X = vectorizer.fit_transform(train_clean_sentences)
X1 = vectorizer.fit_transform(train_clean_sentences1)



y_train = np.zeros(7086)
y_train[0:3996] = 1

y_train1 = np.zeros(7086)
y_train1[0:3996] = 1



modelkmeans = KMeans(n_clusters=2, init='k-means++', max_iter=10000, n_init=100)
modelkmeans.fit(X)
modelkmeans1 = KMeans(n_clusters=2, init='k-means++', max_iter=10000, n_init=100)
modelkmeans1.fit(X1)





print("enter the argument")
test_sentence = []
test_sentence.append(input())

test_clean_sentence = []
for test in test_sentence:
    cleaned_test = clean(test)
    cleaned = ' '.join(cleaned_test)
    cleaned = re.sub(r"\d+","",cleaned)
    test_clean_sentence.append(cleaned)

Test = vectorizer.transform(test_clean_sentence)


true_test_labels = ['con','pro']
true_test_labels1 = ['phone','capital punishment']
predicted_labels_kmeans = modelkmeans.predict(Test)
predicted_labels_kmeans = modelkmeans1.predict(Test)

print("it is a ",true_test_labels[predicted_labels_kmeans[0]],"argument from",true_test_labels1[predicted_labels_kmeans[0]])



fp = open('./test.txt')
data = fp.read()
print("\nOriginal string:")
print(data)

file=open('./test.txt','r');
lines=file.read();
stringarray = re.split(r"[.!?]", lines)
for i in stringarray[:-2]:
    print(i);
    i=[i]
    test_dtm = vectorizer.transform(i)
    predicted_labels_kmeans = modelkmeans.predict(test_dtm)
    predicted_labels_kmeans = modelkmeans1.predict(test_dtm)
    print("it is a ",true_test_labels[predicted_labels_kmeans[0]],"argument from",true_test_labels1[predicted_labels_kmeans[0]])
